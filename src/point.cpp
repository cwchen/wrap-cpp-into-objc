/* point.cpp */
#include <cmath>
#include "point.hpp"


mylib::Point::Point(double x, double y)
{
    setX(x);
    setY(y);
}

double mylib::Point::x()
{
    return _x;
}

void mylib::Point::setX(double x)
{
    _x = x;
}

double mylib::Point::y()
{
    return _y;
}

void mylib::Point::setY(double y)
{
    _y = y;
}

double mylib::Point::distanceBetween(Point *p, Point *q)
{
    double dx = p->x() - q->x();
    double dy = p->y() - q->y();

    return sqrt(dx * dx + dy * dy);
}