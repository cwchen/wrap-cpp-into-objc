/* objcpoint.mm */
#import <Foundation/Foundation.h>
#include <cstdlib>
#include "point.hpp"
#include "objcpoint.h"

struct objcpoint_private_t {
    mylib::Point *pt;
};


@interface ObjCPoint ()
-(objcpoint_private_t *) fields;
@end

@implementation ObjCPoint
+(double) distanceBetween:(ObjCPoint *)p andPoint:(ObjCPoint *)q
{
    return mylib::Point::distanceBetween([p fields]->pt, [q fields]->pt);
}

-(instancetype) init
{
    return [self initWithX:0.0 Y:0.0];
}

-(instancetype) initWithX:(double)x Y:(double)y
{
    if (!self)
        return self;

    fields = \
        (objcpoint_private_t *) malloc(sizeof(objcpoint_private_t));
    if (!fields) {
        [self release];
        self = nil;
        return self;
    }

    fields->pt = new mylib::Point(x, y);
    if (!(fields->pt)) {
        free(fields);
        [self release];
        self = nil;
        return self;
    }

    return self;
}

-(void) dealloc
{
    if (!self)
        return;

    delete (fields->pt);

    if (fields)
        free(fields);

    [super dealloc];
}

-(objcpoint_private_t *) fields
{
    return fields;
}

-(double) x
{
    return fields->pt->x();
}

-(void) setX:(double)x
{
    fields->pt->setX(x);
}

-(double) y
{
    return fields->pt->y();
}

-(void) setY:(double)y
{
    fields->pt->setY(y);
}
@end
