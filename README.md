# Wrap C++ into Objective-C

A tiny program demonstrates how to wrap C++ code into Objective-C one.

## System Requirements

* Objective-C compiler (Clang or GCC)
* Cocoa or GNUstep
* GNU Make (for compilation only)

## Usage

Clone the project:

```
$ git clone https://github.com/cwchentw/wrap-cpp-into-objc.git
```

Move your working directory to the root of the repo:

```
$ cd wrap-cpp-into-objc
```

Compile the application:

```
$ make LDFLAGS=-lm
```

Run the compiled program:

```
$ ./dist/program && echo $?
0
```

## Note

MacOS defines a system `Point` class; therefore, the code will fail. 

## Copyright

Copyright (c) 2021 Michelle Chen. Licensed under MIT.
